#!/usr/bin/env python3.9

import argparse
import re

rows = 0
columns = 0
search_field = []
search_words = []


def get_words_from_location(word, r, c):
    words = []
    lenmod = len(word) - 1
    right = c + len(word) <= columns
    left = c - len(word) >= -1
    up = r - len(word) >= -1
    down = r + len(word) <= rows
    upright = up and right
    upleft = up and left
    downright = down and right
    downleft = down and left
    if right:
        words.append((''.join(search_field[r][c:c + len(word)]),
                      f'{r}:{c}', f'{r}:{c + lenmod}'))
    if left:
        words.append((''.join(search_field[r][c::-1][0:len(word)]),
                      f'{r}:{c}', f'{r}:{c - lenmod}'))
    if down:
        words.append((''.join([search_field[r+x][c] for x in range(len(word))]),
                      f'{r}:{c}', f'{r + lenmod}:{c}'))
    if up:
        words.append((''.join([search_field[r-x][c] for x in range(len(word))]),
                      f'{r}:{c}', f'{r - lenmod}:{c}'))
    if downright:
        words.append((''.join([search_field[r+x][c+x] for x in range(len(word))]),
                      f'{r}:{c}', f'{r + lenmod}:{c + lenmod}'))
    if upright:
        words.append((''.join([search_field[r-x][c+x] for x in range(len(word))]),
                      f'{r}:{c}', f'{r - lenmod}:{c + lenmod}'))
    if downleft:
        words.append((''.join([search_field[r+x][c-x] for x in range(len(word))]),
                      f'{r}:{c}', f'{r + lenmod}:{c - lenmod}'))
    if upleft:
        words.append((''.join([search_field[r-x][c-x] for x in range(len(word))]),
                      f'{r}:{c}', f'{r - lenmod}:{c - lenmod}'))
    return words


def get_letter_locations(letter):
    locations = []
    for r in range(rows):
        for c in range(columns):
            if letter == search_field[r][c]:
                locations.append((r, c))
    return locations


def get_location(word):
    first_letter = get_letter_locations(word[0])
    for location in first_letter:
        r, c = location
        words = get_words_from_location(re.sub(' ', '', word), r, c)
        for t in [x for x in words if x[0] == re.sub(' ', '', word)]:
            return f'{word} {t[1]} {t[2]}'


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='This program finds the location of words in a word search puzzle.')
    parser.add_argument('input', help='Path to the puzzle file.', nargs='?',
                        default='input.txt')
    args = parser.parse_args()
    with open(args.input, 'r') as file:
        contents = file.read().split('\n')
    rows, columns = contents[0].split('x')
    rows = int(rows)
    columns = int(columns)
    search_field = [x.split() for x in contents[1:rows+1]]
    search_words = contents[rows + 1:]

    for word in search_words:
        print(get_location(word))
