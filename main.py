#!/usr/bin/env python3.9

import argparse
import re


rows = 0
columns = 0
search_field = []
search_words = []


def get_letter_locations(letter):
    return [x for x, c in enumerate(search_field) if c == letter]


def get_location(word):
    first_letters = get_letter_locations(word[0])
    for first_letter in first_letters:
        # Check Up
        if (first_letter - (len(word) - 1) * columns) >= 0:
            skip = - columns
            limit = first_letter + skip * (len(word) - 1) - 1
            if limit < 0:
                checked_word = ''.join(search_field[first_letter::skip])
            else:
                checked_word = ''.join(search_field[first_letter:limit:skip])
            if checked_word == word:
                last_letter = limit + 1
                return (f'{int(first_letter / columns)}:{first_letter % columns}',
                        f'{int(last_letter / columns)}:{last_letter % columns}')
        # Check Down
        if rows * columns > (first_letter + (len(word) - 1) * columns):
            skip = columns
            limit = first_letter + skip * (len(word) - 1) + 1
            checked_word = ''.join(search_field[first_letter:limit:skip])
            if checked_word == word:
                last_letter = limit - 1
                return (f'{int(first_letter / columns)}:{first_letter % columns}',
                        f'{int(last_letter / columns)}:{last_letter % columns}')
        if len(word) <= columns:
            # Check Right
            if first_letter % columns < (first_letter + len(word) - 1) % columns:
                skip = 1
                limit = first_letter + skip * len(word)
                checked_word = ''.join(search_field[first_letter:limit:skip])
                if checked_word == word:
                    last_letter = limit - 1
                    return (f'{int(first_letter / columns)}:{first_letter % columns}',
                            f'{int(last_letter / columns)}:{last_letter % columns}')
            # Check Upper Right
            if first_letter % columns < (first_letter - (len(word) - 1) * (columns - 1)) % columns and \
                    0 <= first_letter - (len(word) - 1) * (columns - 1):
                skip = - columns + 1
                limit = first_letter + skip * (len(word) - 1) - 1
                if limit < 0:
                    checked_word = ''.join(search_field[first_letter::skip])
                else:
                    checked_word = ''.join(search_field[first_letter:limit:skip])
                if checked_word == word:
                    last_letter = limit + 1
                    return (f'{int(first_letter / columns)}:{first_letter % columns}',
                            f'{int(last_letter / columns)}:{last_letter % columns}')
            # Check Lower Right
            if first_letter % columns < (first_letter + (len(word) - 1) * (columns + 1)) % columns and \
                    rows * columns > first_letter + (len(word) - 1) * (columns + 1):
                skip = columns + 1
                limit = first_letter + skip * (len(word) - 1) + 1
                checked_word = ''.join(search_field[first_letter:limit:skip])
                if checked_word == word:
                    last_letter = limit - 1
                    return (f'{int(first_letter / columns)}:{first_letter % columns}',
                            f'{int(last_letter / columns)}:{last_letter % columns}')

            # Check Left
            if first_letter % columns > (first_letter - len(word) + 1) % columns:
                skip = -1
                limit = first_letter + skip * len(word)
                if limit < 0:
                    checked_word = ''.join(search_field[first_letter::skip])
                else:
                    checked_word = ''.join(search_field[first_letter:limit:skip])
                if checked_word == word:
                    last_letter = limit + 1
                    return (f'{int(first_letter / columns)}:{first_letter % columns}',
                            f'{int(last_letter / columns)}:{last_letter % columns}')
            # Check Upper Left
            if first_letter % columns > (first_letter - (len(word) - 1) * (columns + 1)) % columns and \
                    0 <= first_letter - (len(word) - 1) * (columns + 1):
                skip = - columns - 1
                limit = first_letter + skip * (len(word) - 1) - 1
                if limit < 0:
                    checked_word = ''.join(search_field[first_letter::skip])
                else:
                    checked_word = ''.join(search_field[first_letter:limit:skip])
                if checked_word == word:
                    last_letter = limit + 1
                    return (f'{int(first_letter / columns)}:{first_letter % columns}',
                            f'{int(last_letter / columns)}:{last_letter % columns}')
            # Check Lower Left
            if first_letter % columns > (first_letter + (len(word) - 1) * (columns - 1)) % columns and \
                    rows * columns > first_letter + (len(word) - 1) * (columns - 1):
                skip = columns - 1
                limit = first_letter + skip * (len(word) - 1) + 1
                checked_word = ''.join(search_field[first_letter:limit:skip])
                if checked_word == word:
                    last_letter = limit - 1
                    return (f'{int(first_letter / columns)}:{first_letter % columns}',
                            f'{int(last_letter / columns)}:{last_letter % columns}')


# This program uses a 1D List to represent the Word Search field
if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='This program finds the location of words in a word search puzzle.')
    parser.add_argument('input', help='Path to the puzzle file.', nargs='?',
                        default='input.txt')
    parser.add_argument('--test', help='Validate Word Search Logic', action='store_true')
    args = parser.parse_args()
    if args.test:
        args.input = 'input_test.txt'
    with open(args.input, 'r') as file:
        contents = file.read().split('\n')

    # Get grid dimensions
    rows, columns = contents[0].split('x')
    rows = int(rows)
    columns = int(columns)
    # Get Grid of Letters
    search_field = ' '.join(contents[1:rows+1]).split()
    # Get Search Words
    search_words = contents[rows + 1:]

    word_locations = []
    for word in search_words:
        location = get_location(re.sub(' ', '', word))
        start = None
        stop = None
        if location:
            start, stop = location
        word_locations.append((word, start, stop))

    for word_location in word_locations:
        word, start, stop = word_location
        print(word, start, stop)
    if args.test:
        with open('input_test_key.txt', 'r') as file:
            result_contents = [tuple(x.rsplit(maxsplit=2)) for x in file.read().split('\n')]
        print()
        print('Expected Results:')
        for word_location in result_contents:
            word, start, stop = word_location
            print(word, start, stop)
        assert word_locations == result_contents, f"There is an error in the program logic."
        print("The Actual Results match the Expected Results.")
